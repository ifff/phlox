with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "phlox";
  buildInputs = [ imagemagick pkgconfig chruby ];
}

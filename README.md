# Phlox

A gem for creating generative gradient art. In brief, here's how it works:
1. Three trees of mathematical expressions are randomly generated
2. These expression trees are used to evaluate the (r, g, b) color of each (x, y) pair in the grid
3. The color values for each spot in the grid are turned into pixels on an image
It works surprisingly well, and an infinite amount of interesting images can be made.

## Gallery

Here are some examples of what `phlox` can generate:

![Gradient art](art/a10.png)
![Gradient art](art/a11.png)
![Gradient art](art/a12.png)
![Gradient art](art/a14.png)
![Gradient art](art/a16.png)
![Gradient art](art/a18.png)
![Gradient art](art/a19.png)
![Gradient art](art/a20.png)
![Gradient art](art/a8.png)
![Gradient art](art/a9.png)
![Gradient art](art/17.png)

See the `art/` directory for more images.

## Installation

Add these lines to your application's Gemfile:

```ruby
gem 'phlox'
gem 'rmagick'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install phlox

## Usage

You can use the `Phlox::gen_pic` method to generate a gradient art image. All parameters to this method are
optional and have default values.
```ruby
gem 'phlox'

# generate a 255x255 gradient, scale it by 3, and write it to "out.png"
Phlox::gen_pic width: 255, height: 255, fpath: "out.png", scale: 3
```
## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/ifff/phlox.

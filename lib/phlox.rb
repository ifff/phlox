require "phlox/version"
require "phlox/graphics"
require "phlox/gradient_gen"

module Phlox
  def self.gen_pic width: 255, height: 255, fpath: "out.png", scale: 1
    graphics = Graphics.new width, height
    gradient = GradientGen::gen_gradient

    graphics.fill gradient
    graphics.write fpath, scale
  end
end

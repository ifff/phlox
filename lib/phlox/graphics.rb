require "rmagick"

module Phlox
  class Graphics
    def initialize(width=255, height=255)
      @width, @height = width, height
      @img = Magick::Image.new @width, @height
    end

    # Fill the image with a pixel gradient determined by the proc argument.
    # The proc should yield a 3-element list of [r, g, b] values for each location.
    def fill grad_prc
      # make an array to hold the values
      data = Array.new(@width) do |x|
        Array.new(@height) do |y|
          grad_prc.call x, y
        end
      end

      # write the color values into the image
      data.each_with_index do |row, y|
        row.each_with_index do |val, x|
          @img.pixel_color y, x, "rgb(#{val.join ', '})"
        end
      end
    end
    
    # Write the image file to `path` with optional scaling.
    def write path, scale=1
      @img.scale(scale).write path
    end
  end
end

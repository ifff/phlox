module Phlox
  module GradientGen
    # Create a Proc that calculates an [r, g, b] triple for given [x, y] coordinates
    # based on randomly-generated calculation functions.
    def self.gen_gradient
      r, g, b = Node.new, Node.new, Node.new

      Proc.new do |x, y|
        [r.run(x, y) % 255,
         g.run(x, y) % 255,
         b.run(x, y) % 255]
      end
    end

    # A node in an expression tree for a pixel color calculation.
    # Initializing a node also generates its child nodes, and you can use `Node#run` to evaluate
    # the expression tree into a numerical value.
    class Node
      def initialize
        # calculate the node's value - it can either be a constant, one of the parameters,
        # a unary function invocation on another node, or a binary invocation on two other nodes
        types = [:param, :param, :const, :unary, :unary, :binary]
        @type = types.sample
        
        case @type
        when :const
          @val = const
        when :param
          @val = param
        when :unary
          unary_call
        when :binary
          binary_call
        end
      end

      # Evaluate the node and its children into a number, given an x and y parameter.
      # Any value that produce an error will equal 0.
      def run x, y
        begin

          # try calculating the value
          case @type
          when :const
            return @val
          when :param
            return {x: x, y: y}[@val]
          when :unary
            v1 = @val1.run x, y
            return run_unary(v1).abs
          when :binary
            v1 = @val1.run x, y
            v2 = @val2.run x, y
            return run_binary(v1, v2).abs
          end
          
        # if something goes wrong, give up and just return 0
        rescue => e
          return 0
        end
      end
      
      # calculate a random constant, from 0 to 255
      def const; rand 255; end

      # choose one of the parameters randomly
      def param; [:x, :y].sample; end

      # make a new argument node and choose a unary function to apply it to
      def unary_call
        @val1 = Node.new
        @fn = unary_fn
      end
      
      def binary_call
        @val1 = Node.new
        @val2 = Node.new
        @fn = binary_fn
      end

      # choose a random unary function
      def unary_fn
        [:sq, :sin, :cos, :mod5, :log2, :tan].sample
      end

      # choose a random binary function
      def binary_fn
        [:mul, :add, :and, :or, :xor, :pow, :sub, :div].sample
      end

      # run @fn on the argument
      def run_unary v1
        case @fn
        when :sq then v1 ** 2
        when :sin then 50 * Math::sin(radians v1)
        when :cos then 50 * Math::cos(radians v1)
        when :mod5 then 10 * (v1.abs % 5)
        when :log2 then 10 * Math::log2(v1.abs)
        when :tan then 50 * Math::tan(radians v1)
        end
      end

      # run @fn on the arguments
      def run_binary v1, v2
        case @fn
        when :mul then v1 * v2
        when :add then v1 + v2
        when :and then v1.to_i & v2.to_i
        when :or then v1.to_i | v2.to_i
        when :xor then v1.to_i ^ v2.to_i
        when :pow then v1 ** (v2 % 10)
        when :sub then v1 - v2
        when :div then v1 / v2.to_f
        end
      end

      # converts a value from degrees to radians
      def radians n
        Math::PI * (n / 180.0)
      end

      def to_s
        case @type
        when :const then @val.to_s
        when :param then @val.to_s
        when :unary then  "#@fn(#@val1)"
        when :binary then "#@fn(#@val1, #@val2)"
        end
      end
    end
  end
end
